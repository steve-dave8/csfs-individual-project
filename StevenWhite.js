/*Resume Page*/

    /*for side navigation bar*/

let rn1 = document.getElementById('rn1') 
let rn2 = document.getElementById('rn2') 
let rn3 = document.getElementById('rn3') 

let height = document.getElementById('bottom').offsetTop

window.onscroll = function() {changeColor()};

function changeColor() {
if ((window.pageYOffset / height ) < 0.4) {
    rn1.className = "highlight";
} else {
    rn1.className = "";
};
if ((window.pageYOffset / height ) < 0.8 && (window.pageYOffset / height ) > 0.4) {
    rn2.className = "highlight";
} else {
    rn2.className = "";
};
if ((window.pageYOffset / height ) > 0.8) {
    rn3.className = "highlight";
} else {
    rn3.className = "";
};
} 

  //for AF video

let popup = document.getElementById("AFpopup");

function OpenVideo() {                         
  popup.style.visibility = "visible";
};

function CloseVideo() {                          
  popup.style.visibility = "hidden";
};

    /*for slideshow*/

let slideIndex = 1;
showSlide(slideIndex)

function openLightbox() {
  document.getElementById('Lightbox').style.display = 'block';
}

function closeLightbox() {
  document.getElementById('Lightbox').style.display = 'none';
};

function changeSlide(n) {
  showSlide(slideIndex += n);
};

function toSlide(n) {
  showSlide(slideIndex = n);
};

function showSlide(n) {
  const slides = document.getElementsByClassName('slide');

  if (n > slides.length) {
    slideIndex = 1;	
  };
  
  if (n < 1) {
    slideIndex = slides.length;
  };

  for (let i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  };
  
  slides[slideIndex - 1].style.display = 'block';
};

window.addEventListener("keydown", function (event){
  switch (event.key){
    case "ArrowLeft":
    case "Left":
        changeSlide(-1);
      break;
    case "ArrowRight":
    case "Right":
        changeSlide(1);
      break;
    case "Escape":
    case "Esc":
        closeLightbox();
      break;
  };
})

//Contact Page

function validateN(form) {
  let nameError = document.getElementById('nameError') ;
      if (form.name.value == "" ) {
          nameError.innerHTML = "Please enter your name" ;
      } else {
          nameError.innerHTML = "" ;
      };
};

function validateE(form){
  let emailError = document.getElementById('emailError') ;
  let atpos = document.getElementById('email').value.indexOf("@") ;
      if (form.email.value == "") {
          emailError.innerHTML = "Please enter your email" ;
      } else if (atpos<1) {
          emailError.innerHTML = "Please enter a valid email" ;
      } else {
          emailError.innerHTML = "" ;
      };
};

function finalValidate() {
  let nameError = document.getElementById('nameError') ;
  let emailError = document.getElementById('emailError') ;
  let formError = document.getElementById('formError') ;
  if (nameError.innerHTML != "" || emailError.innerHTML != "" || document.getElementById('message').value == "") {
      event.preventDefault();
      formError.innerHTML = "Please fill out all fields" ;
  } else {
    alert("Thank you for your message.");
  };
};



